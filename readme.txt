Group members:
Just me.


Project Description:
This project is for the final project for the course ISTA 350. The project basically
contains 2 parts: web crawling & plotting.

The web crawling part is implemented in the crawler.py. When running functions from
this module, all the data are collected live.

The plotting part is implemented in the plot.py.

The last module is driver.py. This behaves as a client and uses the API implemented
in the other modules.


Usage:
python3 driver.py