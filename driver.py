"""
This module behaves like a client-side. It uses the crawler API as well as the plot API, combine them together and shows
the plots.
"""
from crawler import *
from plot import *


def main():
    df_gdp = crawler_GDP()
    plot_GDP(df_gdp)
    plt.show(block=False)

    df = GDP_continents()
    plot_growth(df)
    plt.show(block=False)

    dict_gdp_per_capita = crawler_GDP_per_capita()
    plot_GDP_per_capita(dict_gdp_per_capita)
    plt.show()


if __name__ == '__main__':
    main()
