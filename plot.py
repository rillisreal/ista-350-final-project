"""
This module is used to do the plot for the given data. It includes a scatter plot for  GDP per Country for year 2021 vs
2020, A box-plot for GDP per capita for different countries for year 2021, and a scatter plot for the growth of total
GDP by region.
"""
import matplotlib.pyplot as plt
import math
import pandas as pd
from scipy.stats import linregress
import numpy as np


def train_model(x, y):
    """
    train the linear regression model and return all the required params
    """
    model = linregress(x, y)
    return model.slope, model.intercept, math.pow(model.rvalue, 2), model.pvalue


def plot_GDP(df):
    """
    A scatter plot for GDP per Country for year 2021 vs 2020 plus a linear regression will be plotted and by this
    function. You have to invoke plt.show() to actually show the plot.
    Note, we are not plotting GDP for all the countries. We are focusing on the US or China for too long. In this plot,
    we sampled some smaller countries.
    """
    # -------------- plotting the data --------------
    # random sample the counties in the middle area and convert the unit to be trillion
    x = df['GDP_2020'][:100:][3::3].astype(float) / 1e6
    y = df['GDP_2021_estimate'][:100:][3::3].astype(float) / 1e6

    fig = plt.figure()
    ax = plt.gca()
    ax.scatter(
        x=x,
        y=y,
        label="Random Selected Countries",
        s=10
    )

    slope, intercept, rvalue2, pvalue = train_model(x, y)
    year_range = np.arange(1e-6, max(x), 1e-3)
    regression = [slope * year + intercept for year in year_range]
    pd.Series(
        data=regression,
        index=year_range
    ).plot(
        label="linear regression for GDP (2021 vs 2020)",
        color='goldenrod'
    )

    # -------------- styling the plot --------------
    # https://matplotlib.org/3.1.1/tutorials/colors/colors.html
    fig.patch.set_facecolor('xkcd:chocolate')

    ax.tick_params(axis='x', colors='khaki', labelsize=15)
    ax.tick_params(axis='y', colors='khaki', labelsize=15)
    plt.legend(fontsize=12)

    ax.spines['top'].set_color('khaki')
    ax.spines['left'].set_color('khaki')
    ax.spines['bottom'].set_color('khaki')
    ax.spines['right'].set_color('khaki')
    ax.set_facecolor("#0f0f0f80")

    plt.xlabel('GDP 2020 (US$ trillion)', fontsize=18, color='khaki')
    plt.ylabel('GDP 2021 Estimation (US$ trillion)', fontsize=18, color='khaki')
    plt.title('GDP 2021 vs 2020', fontsize=20, color='khaki')
    plt.tight_layout()


def plot_one_box(ax, c, country, position):
    """
    Used to plot one single box in the boxplot.
    """
    ax.boxplot(country, positions=[position],
               boxprops=dict(color=c),
               capprops=dict(color=c),
               whiskerprops=dict(color=c),
               flierprops=dict(color=c, markeredgecolor=c),
               medianprops=dict(color=c),
               )


def plot_GDP_per_capita(dict_gdp_per_capita):
    """
    A box-plot for GDP per capita for different countries for year 2021 will be plotted and by this function. You have
    to invoke plt.show() to actually show the plot.
    """
    # -------------- plotting the data --------------
    Europe = dict_gdp_per_capita['Europe']
    Asia = dict_gdp_per_capita['Asia']
    Americas = dict_gdp_per_capita['Americas']
    Oceania = dict_gdp_per_capita['Oceania']
    Africa = dict_gdp_per_capita['Africa']

    fig = plt.figure()
    ax = plt.gca()

    plot_one_box(ax, 'teal', Europe, 0)
    plot_one_box(ax, 'blue', Asia, 1)
    plot_one_box(ax, 'chocolate', Americas, 2)
    plot_one_box(ax, 'magenta', Oceania, 3)
    plot_one_box(ax, 'indigo', Africa, 4)

    # -------------- styling the plot --------------
    # https://matplotlib.org/3.1.1/tutorials/colors/colors.html
    fig.patch.set_facecolor('lavender')

    ax.tick_params(axis='x', colors='xkcd:maroon', labelsize=15)
    ax.tick_params(axis='y', colors='xkcd:maroon', labelsize=15)

    ax.spines['top'].set_color('xkcd:maroon')
    ax.spines['left'].set_color('xkcd:maroon')
    ax.spines['bottom'].set_color('xkcd:maroon')
    ax.spines['right'].set_color('xkcd:maroon')
    ax.set_facecolor("ivory")

    ax.set_xticklabels(['Europe', 'Asia', 'Americas', 'Oceania', 'Africa'], fontsize=14)

    plt.xlabel('Countries Grouped by Continent (US$)', fontsize=18, color='xkcd:maroon')
    plt.ylabel('2021 GDP Per Capita (US$)', fontsize=18, color='xkcd:maroon')
    plt.title('GDP per Capita Boxed by Continent', fontsize=20, color='xkcd:maroon')
    plt.tight_layout()


def plot_growth(df):
    """
    Plot the growth of total GDP by region.  You have to invoke plt.show() to actually show the plot.
    """
    df = df / 1e3
    Europe = df['Europe']
    North_America = df['North America']
    Sub_Saharan_Africa = df['Sub-Saharan Africa']
    Latin_America = df['Latin America']
    South_Asia = df['South Asia']

    fig = plt.figure()
    ax = plt.gca()

    Europe.plot(color='teal', style='.')
    North_America.plot(color='chocolate', style='D')
    Sub_Saharan_Africa.plot(color='indigo', style='*')
    Latin_America.plot(color='magenta', style='+')
    South_Asia.plot(color='blue', style='^')

    # -------------- styling the plot --------------
    # https://matplotlib.org/3.1.1/tutorials/colors/colors.html
    fig.patch.set_facecolor('tan')

    ax.tick_params(axis='x', colors='xkcd:maroon', labelsize=15)
    ax.tick_params(axis='y', colors='xkcd:maroon', labelsize=15)

    ax.spines['top'].set_color('xkcd:maroon')
    ax.spines['left'].set_color('xkcd:maroon')
    ax.spines['bottom'].set_color('xkcd:maroon')
    ax.spines['right'].set_color('xkcd:maroon')
    ax.set_facecolor("ivory")

    plt.legend()
    plt.xlabel('Year', fontsize=18, color='xkcd:maroon')
    plt.ylabel('Gross GDP (US$ trillion)', fontsize=18, color='xkcd:maroon')
    plt.title('GDP VS Year by Region ', fontsize=20, color='xkcd:maroon')
    plt.tight_layout()
