"""
This module implements 3 web crawler to collect live data for different website. It doesn't provide any plot.
"""
import collections
import requests
from bs4 import BeautifulSoup
import pandas as pd


def crawler_GDP():
    """
    extract the data GDP by country data from the below url
    :return: DataFrame
    """
    url = 'https://en.wikipedia.org/wiki/List_of_countries_by_GDP_(nominal)'
    gdp_21, gdp_20 = [], []
    source_code = requests.get(url)
    plain_text = source_code.text
    soup = BeautifulSoup(plain_text, features="html.parser")
    prev = ''
    # the numeric data we want is stored with td tag
    for td in soup.findAll("td"):
        if not td.string:
            continue
        if td.string == "2021":
            gdp_21.append(int(prev.replace(",", "")))
        if td.string == "2020":
            # if we missed the 2021 data
            if len(gdp_21) < len(gdp_20):
                continue
            gdp_20.append(int(prev.replace(",", "")))
        prev = td.string

    # convert list of list to DataFrame
    return pd.DataFrame(
        data=list(zip(gdp_20, gdp_21)),
        columns=["GDP_2020", "GDP_2021_estimate"]
    )


def crawler_GDP_per_capita():
    """
    get the GDP per capita for countries. The returned data are group by continent (Europe, Americas, Asia, Africa)
    :return: dictionary of series
    """
    url = 'https://en.wikipedia.org/wiki/List_of_countries_by_GDP_(nominal)_per_capita'
    # Europe, Americas, Asia, Africa = [], [], [], []
    GDP_per_capita = collections.defaultdict(list)
    source_code = requests.get(url)
    plain_text = source_code.text
    soup = BeautifulSoup(plain_text, features="html.parser")
    prev = ''
    country = ''
    # the numeric data we want is stored with td tag
    for td in soup.findAll("td"):
        if not td.string:
            continue
        if td.string == "2021":
            if True in [char.isdigit() for char in prev]:
                GDP_per_capita[country].append(int(prev.replace(",", "")))

        country = prev
        prev = td.string

    dict_GDP_per_capita = dict()
    for k, v in GDP_per_capita.items():
        dict_GDP_per_capita[k] = pd.Series(name=k, data=v)

    return dict_GDP_per_capita


def GDP_by_continent(url):
    """
    Given an URL, collect the GDP vs year written in the URL.
    :param url: url used to do web crawling
    :return: Series
    """
    source_code = requests.get(url)
    plain_text = source_code.text
    soup = BeautifulSoup(plain_text, features="html.parser")
    prev = ''
    country = ''
    # the numeric data we want is stored with td tag
    year = []
    GDP = []
    for td in soup.findAll("td"):
        if not td.string:
            continue
        if td.string.startswith('$'):
            if prev.isdigit():
                year.append(int(prev))
                GDP.append(float(td.string[1:-1:].replace(",", "")))

        prev = td.string

    return pd.Series(data=GDP, index=year)


def GDP_continents():
    """
    get the GDP (billion US$) vs year for 5 regions
    :return:
    """
    url = 'https://www.macrotrends.net/countries/EMU/euro-area/gdp-gross-domestic-product'
    Euro = GDP_by_continent(url)
    url = 'https://www.macrotrends.net/countries/NAC/north-america/gdp-gross-domestic-product'
    North_America = GDP_by_continent(url)
    url = 'https://www.macrotrends.net/countries/SSF/sub-saharan-africa-/gdp-gross-domestic-product'
    Africa = GDP_by_continent(url)
    url = 'https://www.macrotrends.net/countries/LCN/latin-america-caribbean-/gdp-gross-domestic-product'
    Latin_America = GDP_by_continent(url)
    url = 'https://www.macrotrends.net/countries/SAS/south-asia/gdp-gross-domestic-product'
    South_Asia = GDP_by_continent(url)

    return pd.DataFrame(
        data={
            "Europe": Euro,
            "North America": North_America,
            "Sub-Saharan Africa": Africa,
            "Latin America": Latin_America,
            "South Asia": South_Asia
        },
        index=Euro.index
    )
